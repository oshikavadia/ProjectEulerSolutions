import qualified Data.Set as Set


-- (1+..+100)^2-(1^2+..+100^2)

getS::Integer->Integer
getS n= (n*(n+1)*(2*n+1))`div`6

thingy:: Integer->Integer
thingy n= ((n*(n+1)`div`2)^2) - (getS n)


-- smallest multiple, nothing to brag about
smallestMultiple :: Integer
smallestMultiple = lcm (lcm (lcm (lcm (lcm (lcm (lcm (lcm (lcm (lcm 2520 11) 12) 13) 14) 15) 16) 17) 18) 19) 20


-- Collazt Sequence
collatzSequence a 
 | a==1 = [1]
 | even a = [a] ++ collatzSequence (a `div` 2)
 | odd a = [a] ++ collatzSequence (a*3+1)
 
-- getLongestChain= foldr 


generate :: Int -> [[Int]]
generate 0 = []
generate n = generate (n-2) ++ [([x | x<- [1..(n-2)],n>=(2*x), mod (n*n - 2*x*n) (2*n-2*x) == 0])]

-- then x2 to find the answer => 840 

generate2:: Int -> Int -> Int -> Int
generate2 0 _ maxn=maxn 
generate2 n max maxn
 | a> max = generate2 (n-2) a n
 | otherwise = generate2 (n-2) max maxn
 where a = length [x | x<- [1..(n-2)],n>=(2*x), mod (n*n - 2*x*n) (2*n-2*x) == 0]
 
problem39::Int
problem39 = generate2 1000 0 0

sprial :: Int ->Int-> [Int] 
sprial n recur
 | n== 1002 = []
 | n== 1 = (sprial (n+1) 1)++ [1]
 | otherwise = (sprial(n+2) (recur+ (n*4))) ++ [recur+ (n*4), recur+ (n*3),recur + (n*2),recur+n]
 
tach :: Integer -> Integer
tach n | even n = 0
              | n `rem` 5 == 0 = 0
              | otherwise = head [p | p <- [1..], (10^p - 1) `rem` n == 0] 
wrap26= maximum (map (tach) [1,3..1000])

p29= Set.size (Set.fromList (concat $ map (\x -> map (x^) b) a))

