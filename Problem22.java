/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package othercraic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.print.Collation;

/**
 *
 * @author LmThu
 */
public class Problem22 {
    
    static List<String> nameList = new LinkedList<>();
    
    static int getPoint(String a){
        int sum=0;
        for(int i=0;i<a.length();i++){
            sum+=(a.charAt(i)-64);
        }
        return sum; //A is 1
    }
    
    public static void main(String[] args) {
       
        BufferedReader br=null;
        try {
            String filename="p022_names.txt";
            File f= new File(filename);
            br = new BufferedReader(new FileReader(f));
//            System.out.println(br.readLine());
            
            String[] thingy=(br.readLine()).split(",");
            nameList=Arrays.asList(thingy);
            for (int i=0;i<nameList.size();i++){
                nameList.set(i, nameList.get(i).substring(1, nameList.get(i).length()-1));
            }
            Collections.sort(nameList);
            int sum=0;
            System.out.println(nameList);
            for(int i=0;i<nameList.size();i++){
                sum+=(getPoint(nameList.get(i))*(i+1));
            }
            System.out.println(sum);
            br.close();
            
        } catch (FileNotFoundException ex) {
            System.out.println("File not found");
        } catch (IOException ex) {
            System.out.println("something wrong with the buffer");
        } finally {
            try {
                br.close();
            } catch (IOException ex) {
                System.out.println("nah, it's just not gonna work");

            }
        }
        
        
        
    }
}
