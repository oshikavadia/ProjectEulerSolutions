/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package othercraic;

import java.util.ArrayList;

/**
 *
 * @author LmThu
 */
public class Problem21 {
    public static void main(String[] args) {
        int n=10000;
//        ArrayList<Integer> amicableNumber= new ArrayList<>();
        int sum=0;
        int temp;
        for(int i=0;i<n;i++){
            temp=getSumDivisor(i);
            if ((getSumDivisor(temp)==i) && (temp>i)){
                System.out.println(i+" "+temp);
                sum+=(i+temp);            
            }
        }
        System.out.println(sum);
    }
    
    public static int getSumDivisor(int n){
        int sum=1;
        for (int i=2; i<n;i++){
            if(n%i==0){
                sum=sum+i;
            }
        }
        return sum;

    }
}
