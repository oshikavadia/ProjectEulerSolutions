/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package othercraic;

import java.math.BigInteger;

/**
 *
 * @author LmThu
 */
public class Problem16 {
    
    public static void main(String[] args) {
        BigInteger a= new BigInteger("2");
        BigInteger two= new BigInteger("2");

        for (BigInteger i=BigInteger.ONE;i.compareTo(new BigInteger("1000"))!=0;i=i.add(BigInteger.ONE)){
            a=a.multiply(two);
        }
        System.out.println(a);
        BigInteger sum=BigInteger.ZERO;
        while (a.compareTo(BigInteger.ZERO) > 0) {
            sum =sum.add(a.mod(new BigInteger("10")));
            a=a.divide(new BigInteger("10"));
        }
        System.out.println(sum);
    }
    
}
