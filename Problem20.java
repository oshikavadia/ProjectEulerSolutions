/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package othercraic;

import java.math.BigInteger;

/**
 *
 * @author LmThu
 */
public class Problem20 {
    public static void main(String[] args) {
        BigInteger a= BigInteger.ONE;
        
        for (BigInteger i=BigInteger.ONE;i.compareTo(new BigInteger("100"))!=0;i=i.add(BigInteger.ONE)){
            a=a.multiply(i);
        }
        System.out.println(a);
        BigInteger sum=BigInteger.ZERO;
        while (a.compareTo(BigInteger.ZERO) > 0) {
            sum =sum.add(a.mod(new BigInteger("10")));
            a=a.divide(new BigInteger("10"));
        }
        System.out.println(sum);
    }
}
